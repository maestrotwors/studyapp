/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app/app.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const nestjs_kafka_1 = __webpack_require__("@rob3000/nestjs-kafka");
const courses_service_1 = __webpack_require__("./src/app/courses.service.ts");
let AppController = class AppController {
    constructor(coursesService, client) {
        this.coursesService = coursesService;
        this.client = client;
    }
    onModuleInit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.client.subscribeToResponseOf("hero.kill.dragon", this);
            console.log('cources1111 init1');
        });
    }
    getWorld(data, key, offset, timestamp, partition, headers) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log(data);
            console.log(key);
            console.log(offset);
            console.log(timestamp);
            console.log(partition);
            console.log(headers);
        });
    }
};
tslib_1.__decorate([
    (0, nestjs_kafka_1.SubscribeTo)('hero.kill.dragon'),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, Object, Number, Number, Number, typeof (_c = typeof nestjs_kafka_1.IHeaders !== "undefined" && nestjs_kafka_1.IHeaders) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", typeof (_d = typeof Promise !== "undefined" && Promise) === "function" ? _d : Object)
], AppController.prototype, "getWorld", null);
AppController = tslib_1.__decorate([
    (0, common_1.Controller)(),
    tslib_1.__param(1, (0, common_1.Inject)("HERO_SERVICE")),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof courses_service_1.CoursesService !== "undefined" && courses_service_1.CoursesService) === "function" ? _a : Object, typeof (_b = typeof nestjs_kafka_1.KafkaService !== "undefined" && nestjs_kafka_1.KafkaService) === "function" ? _b : Object])
], AppController);
exports.AppController = AppController;


/***/ }),

/***/ "./src/app/app.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const app_controller_1 = __webpack_require__("./src/app/app.controller.ts");
const courses_service_1 = __webpack_require__("./src/app/courses.service.ts");
const nestjs_kafka_1 = __webpack_require__("@rob3000/nestjs-kafka");
const common_1 = __webpack_require__("@nestjs/common");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [
            nestjs_kafka_1.KafkaModule.register([
                {
                    name: "HERO_SERVICE",
                    options: {
                        client: {
                            clientId: "hero",
                            brokers: [`${dev_env_1.env.KAFKA_HOST}:${dev_env_1.env.KAFKA_PORT}`]
                        },
                        consumer: {
                            groupId: "hero-consumer",
                            heartbeatInterval: 0
                        }
                    }
                }
            ])
        ],
        controllers: [app_controller_1.AppController],
        providers: [courses_service_1.CoursesService]
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/courses.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CoursesService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
let CoursesService = class CoursesService {
    constructor() { }
};
CoursesService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [])
], CoursesService);
exports.CoursesService = CoursesService;


/***/ }),

/***/ "../../../dev.env.ts":
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.env = void 0;
exports.env = {
    DATABASE_URL: "postgres://myuser:mypassword@localhost:5432/median-db",
    JWT_ACCESS_TOKEN_SECRET: "1111ggfdsde3bfdsly!@",
    JWT_ACCESS_TOKEN_DURATION: "20m",
    JWT_REFRESH_TOKEN_SECRET: "222ggfdsde3bfdsly#$@!!",
    JWT_REFRESH_TOKEN_DURATION: "14d",
    HASH_ROUNDS: 5,
    REDIS_HOST: "localhost",
    REDIS_PORT: 6379,
    REDIS_PASSWORD: "1234567890",
    REDIS_TTL: 0,
    KAFKA_HOST: "localhost",
    KAFKA_PORT: 29092
};


/***/ }),

/***/ "@nestjs/common":
/***/ ((module) => {

module.exports = require("@nestjs/common");

/***/ }),

/***/ "@nestjs/core":
/***/ ((module) => {

module.exports = require("@nestjs/core");

/***/ }),

/***/ "@nestjs/microservices":
/***/ ((module) => {

module.exports = require("@nestjs/microservices");

/***/ }),

/***/ "@rob3000/nestjs-kafka":
/***/ ((module) => {

module.exports = require("@rob3000/nestjs-kafka");

/***/ }),

/***/ "tslib":
/***/ ((module) => {

module.exports = require("tslib");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
const app_module_1 = __webpack_require__("./src/app/app.module.ts");
const common_1 = __webpack_require__("@nestjs/common");
const core_1 = __webpack_require__("@nestjs/core");
const microservices_1 = __webpack_require__("@nestjs/microservices");
function bootstrap() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.createMicroservice(app_module_1.AppModule, 
        /*{
            transport: Transport.TCP,
            options: {
                host: "::",
                port: 3010
            }
        },*/
        {
            name: "KAFKA_MICROSERVICE",
            transport: microservices_1.Transport.KAFKA,
            options: {
                client: {
                    clientId: "hero2",
                    brokers: ["localhost:29092"]
                },
                consumer: {
                    groupId: "test-consumer"
                }
            }
        });
        yield app.listen();
        common_1.Logger.log(`🚀 Microservice courses is running`);
    });
}
bootstrap();

})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map