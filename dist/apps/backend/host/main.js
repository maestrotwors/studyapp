/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/app/app.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AppModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const articles_module_1 = __webpack_require__("./src/app/modules/articles/articles.module.ts");
const auth_module_1 = __webpack_require__("./src/app/modules/auth/auth.module.ts");
const courses_module_1 = __webpack_require__("./src/app/modules/courses/courses.module.ts");
const common_1 = __webpack_require__("@nestjs/common");
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            articles_module_1.ArticlesModule,
            courses_module_1.CourseModule
        ],
        controllers: [],
        providers: []
    })
], AppModule);
exports.AppModule = AppModule;


/***/ }),

/***/ "./src/app/decorators/auth.decorator.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Auth = void 0;
const swagger_1 = __webpack_require__("@nestjs/swagger");
const common_1 = __webpack_require__("@nestjs/common");
const auth_guard_1 = __webpack_require__("./src/app/modules/auth/guards/auth.guard.ts");
function Auth(roles) {
    return (0, common_1.applyDecorators)((0, common_1.SetMetadata)("roles", roles), (0, common_1.UseGuards)(auth_guard_1.AuthGuard), (0, swagger_1.ApiUnauthorizedResponse)({ description: "Unauthorized" }));
}
exports.Auth = Auth;


/***/ }),

/***/ "./src/app/modules/articles/articles.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ArticlesController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const swagger_1 = __webpack_require__("@nestjs/swagger");
const auth_decorator_1 = __webpack_require__("./src/app/decorators/auth.decorator.ts");
const articles_service_1 = __webpack_require__("./src/app/modules/articles/articles.service.ts");
const create_article_dto_1 = __webpack_require__("./src/app/modules/articles/dto/create-article.dto.ts");
const update_article_dto_1 = __webpack_require__("./src/app/modules/articles/dto/update-article.dto.ts");
const article_entity_1 = __webpack_require__("./src/app/modules/articles/entities/article.entity.ts");
const roles_1 = __webpack_require__("../../../libs/backend/models/src/lib/auth/roles.ts");
let ArticlesController = class ArticlesController {
    constructor(articlesService) {
        this.articlesService = articlesService;
    }
    create(createArticleDto) {
        return this.articlesService.create(createArticleDto);
    }
    findAll() {
        return this.articlesService.findAll();
    }
    findDrafts() {
        //return this.articlesService.findDrafts();
    }
    findOne(id) {
        return this.articlesService.findOne(+id);
    }
    update(id, updateArticleDto) {
        return this.articlesService.update(+id, updateArticleDto);
    }
    remove(id) {
        return this.articlesService.remove(+id);
    }
};
tslib_1.__decorate([
    (0, common_1.Post)(),
    (0, swagger_1.ApiCreatedResponse)({ type: article_entity_1.ArticleEntity }),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_b = typeof create_article_dto_1.CreateArticleDto !== "undefined" && create_article_dto_1.CreateArticleDto) === "function" ? _b : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "create", null);
tslib_1.__decorate([
    (0, common_1.Get)()
    //@UseGuards(AuthGuard)
    //@ApiOkResponse({ type: ArticleEntity, isArray: true })
    ,
    (0, swagger_1.ApiOkResponse)({ type: article_entity_1.ArticleEntity, isArray: true }),
    (0, auth_decorator_1.Auth)([roles_1.Role.Student]),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "findAll", null);
tslib_1.__decorate([
    (0, common_1.Get)("drafts"),
    (0, swagger_1.ApiOkResponse)({ type: article_entity_1.ArticleEntity, isArray: true }),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "findDrafts", null);
tslib_1.__decorate([
    (0, common_1.Get)(":id"),
    (0, swagger_1.ApiOkResponse)({ type: article_entity_1.ArticleEntity }),
    tslib_1.__param(0, (0, common_1.Param)("id")),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "findOne", null);
tslib_1.__decorate([
    (0, common_1.Patch)(":id"),
    (0, swagger_1.ApiOkResponse)({ type: article_entity_1.ArticleEntity }),
    tslib_1.__param(0, (0, common_1.Param)("id")),
    tslib_1.__param(1, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String, typeof (_c = typeof update_article_dto_1.UpdateArticleDto !== "undefined" && update_article_dto_1.UpdateArticleDto) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "update", null);
tslib_1.__decorate([
    (0, common_1.Delete)(":id"),
    (0, swagger_1.ApiOkResponse)({ type: article_entity_1.ArticleEntity }),
    tslib_1.__param(0, (0, common_1.Param)("id")),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", void 0)
], ArticlesController.prototype, "remove", null);
ArticlesController = tslib_1.__decorate([
    (0, common_1.Controller)("articles"),
    (0, swagger_1.ApiTags)("articles"),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof articles_service_1.ArticlesService !== "undefined" && articles_service_1.ArticlesService) === "function" ? _a : Object])
], ArticlesController);
exports.ArticlesController = ArticlesController;


/***/ }),

/***/ "./src/app/modules/articles/articles.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ArticlesModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const articles_controller_1 = __webpack_require__("./src/app/modules/articles/articles.controller.ts");
const articles_service_1 = __webpack_require__("./src/app/modules/articles/articles.service.ts");
const auth_module_1 = __webpack_require__("./src/app/modules/auth/auth.module.ts");
const common_1 = __webpack_require__("@nestjs/common");
const _be_repository_1 = __webpack_require__("../../../libs/backend/repository/src/index.ts");
const prisma_service_1 = __webpack_require__("../../../libs/backend/repository/src/lib/prisma.service.ts");
let ArticlesModule = class ArticlesModule {
};
ArticlesModule = tslib_1.__decorate([
    (0, common_1.Module)({
        controllers: [articles_controller_1.ArticlesController],
        providers: [articles_service_1.ArticlesService, prisma_service_1.PrismaService],
        imports: [_be_repository_1.PrismaModule, auth_module_1.AuthModule]
    })
], ArticlesModule);
exports.ArticlesModule = ArticlesModule;


/***/ }),

/***/ "./src/app/modules/articles/articles.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


// src/articles/articles.service.ts
var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ArticlesService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const prisma_service_1 = __webpack_require__("../../../libs/backend/repository/src/lib/prisma.service.ts");
// src/articles/articles.service.ts
let ArticlesService = class ArticlesService {
    constructor(prisma) {
        this.prisma = prisma;
    }
    create(createArticleDto) {
        return this.prisma.article.create({ data: createArticleDto });
    }
    findAll() {
        return this.prisma.article.findMany({ where: { published: true } });
    }
    findOne(id) {
        return this.prisma.article.findUnique({ where: { id } });
    }
    update(id, updateArticleDto) {
        return this.prisma.article.update({ where: { id }, data: updateArticleDto });
    }
    remove(id) {
        return this.prisma.article.delete({ where: { id } });
    }
};
ArticlesService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof prisma_service_1.PrismaService !== "undefined" && prisma_service_1.PrismaService) === "function" ? _a : Object])
], ArticlesService);
exports.ArticlesService = ArticlesService;


/***/ }),

/***/ "./src/app/modules/articles/dto/create-article.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CreateArticleDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class CreateArticleDto {
    constructor() {
        this.published = false;
    }
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CreateArticleDto.prototype, "title", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false }),
    tslib_1.__metadata("design:type", String)
], CreateArticleDto.prototype, "description", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], CreateArticleDto.prototype, "body", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false, default: false }),
    tslib_1.__metadata("design:type", Boolean)
], CreateArticleDto.prototype, "published", void 0);
exports.CreateArticleDto = CreateArticleDto;


/***/ }),

/***/ "./src/app/modules/articles/dto/update-article.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.UpdateArticleDto = void 0;
const create_article_dto_1 = __webpack_require__("./src/app/modules/articles/dto/create-article.dto.ts");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class UpdateArticleDto extends (0, swagger_1.PartialType)(create_article_dto_1.CreateArticleDto) {
}
exports.UpdateArticleDto = UpdateArticleDto;


/***/ }),

/***/ "./src/app/modules/articles/entities/article.entity.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


// src/articles/entities/article.entity.ts
var _a, _b;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ArticleEntity = void 0;
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class ArticleEntity {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Number)
], ArticleEntity.prototype, "id", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ArticleEntity.prototype, "title", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)({ required: false, nullable: true }),
    tslib_1.__metadata("design:type", String)
], ArticleEntity.prototype, "description", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", String)
], ArticleEntity.prototype, "body", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", Boolean)
], ArticleEntity.prototype, "published", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_a = typeof Date !== "undefined" && Date) === "function" ? _a : Object)
], ArticleEntity.prototype, "createdAt", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    tslib_1.__metadata("design:type", typeof (_b = typeof Date !== "undefined" && Date) === "function" ? _b : Object)
], ArticleEntity.prototype, "updatedAt", void 0);
exports.ArticleEntity = ArticleEntity;


/***/ }),

/***/ "./src/app/modules/auth/auth.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c, _d, _e, _f, _g;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AuthController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const auth_service_1 = __webpack_require__("./src/app/modules/auth/auth.service.ts");
const logIn_dto_1 = __webpack_require__("./src/app/modules/auth/dto/logIn.dto.ts");
const refresh_token_dto_1 = __webpack_require__("./src/app/modules/auth/dto/refresh-token.dto.ts");
const signup_dto_1 = __webpack_require__("./src/app/modules/auth/dto/signup.dto.ts");
const change_password_dto_1 = __webpack_require__("./src/app/modules/auth/dto/change-password.dto.ts");
const auth_decorator_1 = __webpack_require__("./src/app/decorators/auth.decorator.ts");
const roles_1 = __webpack_require__("../../../libs/backend/models/src/lib/auth/roles.ts");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
    }
    login(body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.authService.logIn(body);
            }
            catch (_a) {
                throw new common_1.UnauthorizedException("Invalid auth data.");
            }
        });
    }
    signup(body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.authService.signup(body);
            }
            catch (_a) {
                throw new common_1.NotAcceptableException();
            }
        });
    }
    changePassword(body, request) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.authService.changePassword(body, request === null || request === void 0 ? void 0 : request['user']);
            }
            catch (_a) {
                throw new common_1.NotAcceptableException();
            }
        });
    }
    logout() {
        return this.authService.logout();
    }
    refreshToken(body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.authService.refreshToken(body.refreshToken);
            }
            catch (_a) {
                throw new common_1.UnauthorizedException("Invalid refresh token");
            }
        });
    }
};
tslib_1.__decorate([
    (0, common_1.Post)("login"),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_b = typeof logIn_dto_1.LogInDto !== "undefined" && logIn_dto_1.LogInDto) === "function" ? _b : Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "login", null);
tslib_1.__decorate([
    (0, common_1.Post)("signup"),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_c = typeof signup_dto_1.SignUpDto !== "undefined" && signup_dto_1.SignUpDto) === "function" ? _c : Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "signup", null);
tslib_1.__decorate([
    (0, auth_decorator_1.Auth)([roles_1.Role.Student, roles_1.Role.Teacher]),
    (0, common_1.Post)("change-password"),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__param(1, (0, common_1.Req)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_d = typeof change_password_dto_1.ChangePasswordDto !== "undefined" && change_password_dto_1.ChangePasswordDto) === "function" ? _d : Object, typeof (_e = typeof Request !== "undefined" && Request) === "function" ? _e : Object]),
    tslib_1.__metadata("design:returntype", Promise)
], AuthController.prototype, "changePassword", null);
tslib_1.__decorate([
    (0, common_1.Get)("logout"),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", void 0)
], AuthController.prototype, "logout", null);
tslib_1.__decorate([
    (0, common_1.Post)("refresh-token"),
    tslib_1.__param(0, (0, common_1.Body)()),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [typeof (_f = typeof refresh_token_dto_1.RefreshTokenDto !== "undefined" && refresh_token_dto_1.RefreshTokenDto) === "function" ? _f : Object]),
    tslib_1.__metadata("design:returntype", typeof (_g = typeof Promise !== "undefined" && Promise) === "function" ? _g : Object)
], AuthController.prototype, "refreshToken", null);
AuthController = tslib_1.__decorate([
    (0, common_1.Controller)("auth"),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _a : Object])
], AuthController);
exports.AuthController = AuthController;


/***/ }),

/***/ "./src/app/modules/auth/auth.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AuthModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const auth_controller_1 = __webpack_require__("./src/app/modules/auth/auth.controller.ts");
const auth_service_1 = __webpack_require__("./src/app/modules/auth/auth.service.ts");
const _be_cache_1 = __webpack_require__("../../../libs/backend/cache/src/index.ts");
const date_service_1 = __webpack_require__("../../../libs/backend/system/src/lib/date.service.ts");
const jwt_1 = __webpack_require__("@nestjs/jwt");
const common_1 = __webpack_require__("@nestjs/common");
const _be_repository_1 = __webpack_require__("../../../libs/backend/repository/src/index.ts");
const prisma_service_1 = __webpack_require__("../../../libs/backend/repository/src/lib/prisma.service.ts");
const _be_system_1 = __webpack_require__("../../../libs/backend/system/src/index.ts");
const token_service_1 = __webpack_require__("./src/app/modules/auth/token.service.ts");
const user_repository_1 = __webpack_require__("./src/app/modules/auth/repositories/user.repository.ts");
const user_service_1 = __webpack_require__("./src/app/modules/auth/user.service.ts");
let AuthModule = class AuthModule {
};
AuthModule = tslib_1.__decorate([
    (0, common_1.Module)({
        controllers: [auth_controller_1.AuthController],
        providers: [auth_service_1.AuthService, token_service_1.TokenService, user_service_1.UserService, user_repository_1.UserRepository, prisma_service_1.PrismaService, date_service_1.DateService],
        imports: [_be_system_1.SystemModule, jwt_1.JwtModule.register({}), _be_repository_1.PrismaModule, _be_cache_1.CacheModule],
        exports: [auth_service_1.AuthService]
    })
], AuthModule);
exports.AuthModule = AuthModule;


/***/ }),

/***/ "./src/app/modules/auth/auth.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b, _c, _d;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AuthService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const bcrypt_1 = __webpack_require__("bcrypt");
const cache_service_1 = __webpack_require__("../../../libs/backend/cache/src/lib/cache.service.ts");
const date_service_1 = __webpack_require__("../../../libs/backend/system/src/lib/date.service.ts");
const token_service_1 = __webpack_require__("./src/app/modules/auth/token.service.ts");
const user_service_1 = __webpack_require__("./src/app/modules/auth/user.service.ts");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
let AuthService = class AuthService {
    constructor(tokenService, userService, dateService, cacheService) {
        this.tokenService = tokenService;
        this.userService = userService;
        this.dateService = dateService;
        this.cacheService = cacheService;
    }
    logIn({ login, password }) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.loginWithCache(login, password);
        });
    }
    logout() { }
    refreshToken(token) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const verificedToken = yield this.tokenService.verifyRefreshToken(token);
            if (!verificedToken) {
                throw new common_1.UnauthorizedException("Invalid refresh token");
            }
            const user = yield this.userService.getUserById(verificedToken.id);
            if (!user) {
                throw new common_1.UnauthorizedException("Invalid refresh token");
            }
            const tokenDecoded = this.tokenService.decodeToken(token);
            if (this.dateService.compare(user.updatedAt, tokenDecoded === null || tokenDecoded === void 0 ? void 0 : tokenDecoded["uat"])) {
                return yield this.generateUserTokens(user);
            }
            throw new common_1.UnauthorizedException("Invalid refresh token");
        });
    }
    verifyAccessToken(token) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const tokenResponse = yield this.tokenService.verifyAccessToken(token);
                if (tokenResponse) {
                    let foundUser = yield this.cacheService.getValueByKey(tokenResponse.login);
                    if (!foundUser) {
                        foundUser = yield this.userService.getUserById(tokenResponse.id);
                    }
                    const tokenDecoded = this.tokenService.decodeToken(token);
                    const isMatch = yield this.dateService.compare(foundUser === null || foundUser === void 0 ? void 0 : foundUser["updatedAt"], tokenDecoded === null || tokenDecoded === void 0 ? void 0 : tokenDecoded["uat"]);
                    if (isMatch) {
                        return foundUser;
                    }
                }
                throw new common_1.UnauthorizedException();
            }
            catch (err) {
                throw new common_1.UnauthorizedException(err.message);
            }
        });
    }
    signup(body) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.userService.signup(body);
        });
    }
    changePassword(data, user) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            let foundUser = yield this.cacheService.getValueByKey(user.login);
            if (!foundUser) {
                foundUser = yield this.userService.getUserByLogin(user.login);
                if (!foundUser) {
                    throw new common_1.UnauthorizedException("Invalid user.");
                }
            }
            const isMatch = yield (0, bcrypt_1.compare)(data.oldPassword, user.password);
            if (isMatch) {
                const newPassword = yield (0, bcrypt_1.hash)(data.newPassword, dev_env_1.env.HASH_ROUNDS);
                const payload = { login: user.login, newPassword };
                const updatedUser = yield this.userService.changePassword(payload);
                this.cacheService.setValue(user.login, updatedUser);
                return {
                    "message": "password changed",
                    "status": "ok"
                };
            }
            else {
                throw new common_1.ForbiddenException("Invalid password.");
            }
        });
    }
    generateUserTokens(user) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return {
                access_token: yield this.tokenService.signAccessToken(user),
                refresh_token: yield this.tokenService.signRefreshToken(user)
            };
        });
    }
    loginWithCache(login, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const userCache = yield this.cacheService.getValueByKey(login);
            if (!!userCache) {
                const user = yield this.userService.verifyUser(userCache, password);
                if (!user) {
                    throw new common_1.UnauthorizedException("Invalid login or password");
                }
                else {
                    return this.generateUserTokens(user);
                }
            }
            return this.loginFromRepository(login, password);
        });
    }
    loginFromRepository(login, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.getUserByCreds(login, password);
            if (!!user) {
                this.cacheService.setValue(user.login, user);
            }
            if (user) {
                return this.generateUserTokens(user);
            }
            else {
                throw new common_1.UnauthorizedException("Invalid login or password");
            }
        });
    }
};
AuthService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof token_service_1.TokenService !== "undefined" && token_service_1.TokenService) === "function" ? _a : Object, typeof (_b = typeof user_service_1.UserService !== "undefined" && user_service_1.UserService) === "function" ? _b : Object, typeof (_c = typeof date_service_1.DateService !== "undefined" && date_service_1.DateService) === "function" ? _c : Object, typeof (_d = typeof cache_service_1.CacheService !== "undefined" && cache_service_1.CacheService) === "function" ? _d : Object])
], AuthService);
exports.AuthService = AuthService;


/***/ }),

/***/ "./src/app/modules/auth/dto/change-password.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.ChangePasswordDataDto = exports.ChangePasswordDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const class_validator_1 = __webpack_require__("class-validator");
class ChangePasswordDto {
}
tslib_1.__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: "oldPassword is empty" }),
    (0, class_validator_1.IsString)(),
    tslib_1.__metadata("design:type", Object)
], ChangePasswordDto.prototype, "oldPassword", void 0);
tslib_1.__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: "newPassword is empty" }),
    (0, class_validator_1.IsString)(),
    tslib_1.__metadata("design:type", Object)
], ChangePasswordDto.prototype, "newPassword", void 0);
exports.ChangePasswordDto = ChangePasswordDto;
class ChangePasswordDataDto {
}
exports.ChangePasswordDataDto = ChangePasswordDataDto;


/***/ }),

/***/ "./src/app/modules/auth/dto/logIn.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.LogInDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const class_validator_1 = __webpack_require__("class-validator");
class LogInDto {
}
tslib_1.__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: "logIn is empty" }),
    (0, class_validator_1.IsString)(),
    tslib_1.__metadata("design:type", Object)
], LogInDto.prototype, "login", void 0);
tslib_1.__decorate([
    (0, class_validator_1.IsNotEmpty)({ message: "password is empty" }),
    (0, class_validator_1.IsString)(),
    tslib_1.__metadata("design:type", Object)
], LogInDto.prototype, "password", void 0);
exports.LogInDto = LogInDto;


/***/ }),

/***/ "./src/app/modules/auth/dto/refresh-token.dto.ts":
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.RefreshTokenDto = void 0;
class RefreshTokenDto {
}
exports.RefreshTokenDto = RefreshTokenDto;


/***/ }),

/***/ "./src/app/modules/auth/dto/signup.dto.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SignUpDto = void 0;
const tslib_1 = __webpack_require__("tslib");
const class_validator_1 = __webpack_require__("class-validator");
const swagger_1 = __webpack_require__("@nestjs/swagger");
class SignUpDto {
}
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: "login is empty" }),
    (0, class_validator_1.IsString)({ message: "login is not string" }),
    tslib_1.__metadata("design:type", Object)
], SignUpDto.prototype, "login", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: "email is empty" }),
    (0, class_validator_1.IsEmail)(),
    tslib_1.__metadata("design:type", Object)
], SignUpDto.prototype, "email", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: "password is empty" }),
    (0, class_validator_1.IsString)({ message: "password is not string" }),
    tslib_1.__metadata("design:type", Object)
], SignUpDto.prototype, "password", void 0);
tslib_1.__decorate([
    (0, swagger_1.ApiProperty)(),
    (0, class_validator_1.IsNotEmpty)({ message: "name is empty" }),
    (0, class_validator_1.IsString)({ message: "name is not string" }),
    tslib_1.__metadata("design:type", Object)
], SignUpDto.prototype, "name", void 0);
exports.SignUpDto = SignUpDto;


/***/ }),

/***/ "./src/app/modules/auth/guards/auth.guard.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.AuthGuard = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const auth_service_1 = __webpack_require__("./src/app/modules/auth/auth.service.ts");
let AuthGuard = class AuthGuard {
    constructor(authService) {
        this.authService = authService;
    }
    canActivate(context) {
        var _a;
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const request = context.switchToHttp().getRequest();
            const auth = (_a = request === null || request === void 0 ? void 0 : request.headers) === null || _a === void 0 ? void 0 : _a.authorization;
            if (auth === undefined) {
                return false;
            }
            const token = auth.split(" ");
            const userInfo = yield this.authService.verifyAccessToken(token[1]);
            if (userInfo) {
                request.user = userInfo;
                return true;
            }
            else {
                return false;
            }
        });
    }
};
AuthGuard = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof auth_service_1.AuthService !== "undefined" && auth_service_1.AuthService) === "function" ? _a : Object])
], AuthGuard);
exports.AuthGuard = AuthGuard;


/***/ }),

/***/ "./src/app/modules/auth/repositories/user.repository.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.UserRepository = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const prisma_service_1 = __webpack_require__("../../../libs/backend/repository/src/lib/prisma.service.ts");
let UserRepository = class UserRepository {
    constructor(db) {
        this.db = db;
    }
    getUserByLogin(login) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.db.user.findUnique({ where: { login } });
            return !user ? null : user;
        });
    }
    getUserById(id) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.db.user.findUnique({ where: { id } });
            return !user ? null : user;
        });
    }
    signup(data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.db.user.create({ data });
        });
    }
    updateUserCreds(payload) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.db.user.update({
                where: { login: payload.login },
                data: { password: payload.newPassword }
            });
        });
    }
};
UserRepository = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof prisma_service_1.PrismaService !== "undefined" && prisma_service_1.PrismaService) === "function" ? _a : Object])
], UserRepository);
exports.UserRepository = UserRepository;


/***/ }),

/***/ "./src/app/modules/auth/token.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.TokenService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const jwt_1 = __webpack_require__("@nestjs/jwt");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
let TokenService = class TokenService {
    constructor(jwtService) {
        this.jwtService = jwtService;
    }
    signAccessToken(user) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const payload = {
                id: user.id,
                uat: user.updatedAt,
                login: user.login
            };
            const options = {
                expiresIn: dev_env_1.env.JWT_ACCESS_TOKEN_DURATION,
                secret: dev_env_1.env.JWT_ACCESS_TOKEN_SECRET
            };
            return yield this.jwtService.signAsync(payload, options);
        });
    }
    signRefreshToken(user) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const payload = {
                id: user.id,
                uat: user.updatedAt
            };
            const options = {
                expiresIn: dev_env_1.env.JWT_REFRESH_TOKEN_DURATION,
                secret: dev_env_1.env.JWT_REFRESH_TOKEN_SECRET
            };
            return yield this.jwtService.signAsync(payload, options);
        });
    }
    verifyRefreshToken(token) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.verifyToken(token, dev_env_1.env.JWT_REFRESH_TOKEN_SECRET);
        });
    }
    verifyAccessToken(token) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.verifyToken(token, dev_env_1.env.JWT_ACCESS_TOKEN_SECRET);
        });
    }
    decodeToken(token) {
        return this.jwtService.decode(token);
    }
    verifyToken(token, secret) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this.jwtService.verifyAsync(token, { secret });
                const currDatetime = new Date().getTime() / 1000;
                if (currDatetime > result.exp) {
                    return false;
                }
                return result;
            }
            catch (_a) {
                return false;
            }
        });
    }
};
TokenService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof jwt_1.JwtService !== "undefined" && jwt_1.JwtService) === "function" ? _a : Object])
], TokenService);
exports.TokenService = TokenService;


/***/ }),

/***/ "./src/app/modules/auth/user.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.UserService = void 0;
const tslib_1 = __webpack_require__("tslib");
const bcrypt_1 = __webpack_require__("bcrypt");
const common_1 = __webpack_require__("@nestjs/common");
const user_repository_1 = __webpack_require__("./src/app/modules/auth/repositories/user.repository.ts");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
let UserService = class UserService {
    constructor(userRepository) {
        this.userRepository = userRepository;
    }
    getUserByCreds(login, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.getUserByLogin(login);
            if (!user) {
                return null;
            }
            const isMatch = yield (0, bcrypt_1.compare)(password, user.password);
            if (isMatch) {
                return user;
            }
            return null;
        });
    }
    getUserById(id) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.getUserById(id);
            if (!user) {
                return null;
            }
            return user;
        });
    }
    getUserByLogin(login) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const user = yield this.userRepository.getUserByLogin(login);
            if (!user) {
                return null;
            }
            return user;
        });
    }
    verifyUser(user, password) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const isMatch = yield (0, bcrypt_1.compare)(password, user.password);
            if (isMatch) {
                return user;
            }
            return false;
        });
    }
    signup(data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            data.password = yield (0, bcrypt_1.hash)(data.password, dev_env_1.env.HASH_ROUNDS);
            return yield this.userRepository.signup(data);
        });
    }
    changePassword(data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.userRepository.updateUserCreds(data);
        });
    }
};
UserService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof user_repository_1.UserRepository !== "undefined" && user_repository_1.UserRepository) === "function" ? _a : Object])
], UserService);
exports.UserService = UserService;


/***/ }),

/***/ "./src/app/modules/courses/courses.controller.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a, _b;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CourseController = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const nestjs_kafka_1 = __webpack_require__("@rob3000/nestjs-kafka");
const courses_service_1 = __webpack_require__("./src/app/modules/courses/courses.service.ts");
let CourseController = class CourseController {
    constructor(coursesService, client) {
        this.coursesService = coursesService;
        this.client = client;
    }
    getAllCourses() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const result = yield this.client.send({
                topic: "hero.kill.dragon",
                messages: [
                    {
                        key: "1",
                        value: "My messageeeeeeeeeeeee"
                    }
                ]
            });
            console.log(result);
            console.log('++++++');
            return this.coursesService.getAllCourses();
        });
    }
};
tslib_1.__decorate([
    (0, common_1.Get)("all"),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", []),
    tslib_1.__metadata("design:returntype", Promise)
], CourseController.prototype, "getAllCourses", null);
CourseController = tslib_1.__decorate([
    (0, common_1.Controller)("course"),
    tslib_1.__param(1, (0, common_1.Inject)("HERO_SERVICE")),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof courses_service_1.CoursesService !== "undefined" && courses_service_1.CoursesService) === "function" ? _a : Object, typeof (_b = typeof nestjs_kafka_1.KafkaService !== "undefined" && nestjs_kafka_1.KafkaService) === "function" ? _b : Object])
], CourseController);
exports.CourseController = CourseController;


/***/ }),

/***/ "./src/app/modules/courses/courses.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


//import { ClientsModule, Transport } from '@nestjs/microservices';
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CourseModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const courses_controller_1 = __webpack_require__("./src/app/modules/courses/courses.controller.ts");
const courses_service_1 = __webpack_require__("./src/app/modules/courses/courses.service.ts");
const nestjs_kafka_1 = __webpack_require__("@rob3000/nestjs-kafka");
const common_1 = __webpack_require__("@nestjs/common");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
let CourseModule = class CourseModule {
};
CourseModule = tslib_1.__decorate([
    (0, common_1.Module)({
        controllers: [courses_controller_1.CourseController],
        providers: [courses_service_1.CoursesService],
        imports: [
            nestjs_kafka_1.KafkaModule.register([
                {
                    name: "HERO_SERVICE",
                    options: {
                        client: {
                            clientId: "hero",
                            brokers: [`${dev_env_1.env.KAFKA_HOST}:${dev_env_1.env.KAFKA_PORT}`]
                        },
                        consumer: {
                            groupId: "hero-consumer",
                            heartbeatInterval: 0
                        }
                    }
                }
            ])
            /*ClientsModule.register([
                /*{
                    name: "ITEM_MICROSERVICE",
                    transport: Transport.TCP,
                    options: { host: "::", port: 3010 }
                },
                {
                    name: "KAFKA_MICROSERVICE",
                    transport: Transport.KAFKA,
                    options: {
                        client: {
                            clientId: "hero",
                            brokers: ["localhost:29092"]
                        },
                        consumer: {
                            groupId: "test-consumer"
                        },
                        producerOnlyMode: true
                    }
                }
            ])*/
        ]
    })
], CourseModule);
exports.CourseModule = CourseModule;


/***/ }),

/***/ "./src/app/modules/courses/courses.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CoursesService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
//import { ClientKafka, ClientProxy } from '@nestjs/microservices';
//import { GetCoursesEvent } from './events/get-courses.event';';
let CoursesService = class CoursesService {
    constructor(
    //@Inject("ITEM_MICROSERVICE")
    //private readonly client: ClientProxy,
    //@Inject("KAFKA_MICROSERVICE")
    //private readonly kafkaClient: ClientKafka
    ) { }
    onModuleInit() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            console.log("before connect");
            //await this.client.connect();
            //await this.kafkaClient.connect();
            console.log("after connect");
        });
    }
    getAllCourses() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            //console.time();
            console.log("______");
            //this.kafkaClient.emit('data', new GetCoursesEvent('5ef34'));
            /*this.client
                .send({ role: "test-param", cmd: "test-command" }, "8888888")
                .subscribe((data) => {
                    console.log(data);
                    console.timeEnd();
                });*/
            return "all courses";
        });
    }
};
CoursesService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__metadata("design:paramtypes", [])
], CoursesService);
exports.CoursesService = CoursesService;


/***/ }),

/***/ "../../../dev.env.ts":
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.env = void 0;
exports.env = {
    DATABASE_URL: "postgres://myuser:mypassword@localhost:5432/median-db",
    JWT_ACCESS_TOKEN_SECRET: "1111ggfdsde3bfdsly!@",
    JWT_ACCESS_TOKEN_DURATION: "20m",
    JWT_REFRESH_TOKEN_SECRET: "222ggfdsde3bfdsly#$@!!",
    JWT_REFRESH_TOKEN_DURATION: "14d",
    HASH_ROUNDS: 5,
    REDIS_HOST: "localhost",
    REDIS_PORT: 6379,
    REDIS_PASSWORD: "1234567890",
    REDIS_TTL: 0,
    KAFKA_HOST: "localhost",
    KAFKA_PORT: 29092
};


/***/ }),

/***/ "../../../libs/backend/cache/src/index.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
tslib_1.__exportStar(__webpack_require__("../../../libs/backend/cache/src/lib/cache.module.ts"), exports);


/***/ }),

/***/ "../../../libs/backend/cache/src/lib/cache.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CacheModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const cache_service_1 = __webpack_require__("../../../libs/backend/cache/src/lib/cache.service.ts");
const dev_env_1 = __webpack_require__("../../../dev.env.ts");
const cache_manager_redis_yet_1 = __webpack_require__("cache-manager-redis-yet");
let CacheModule = class CacheModule {
};
CacheModule = tslib_1.__decorate([
    (0, common_1.Module)({
        controllers: [],
        providers: [cache_service_1.CacheService],
        imports: [
            common_1.CacheModule.register({
                isGlobal: true,
                store: cache_manager_redis_yet_1.redisStore,
                url: `redis://${dev_env_1.env.REDIS_HOST}:${dev_env_1.env.REDIS_PORT}`,
                password: dev_env_1.env.REDIS_PASSWORD,
                ttl: dev_env_1.env.REDIS_TTL
            })
        ],
        exports: [cache_service_1.CacheService]
    })
], CacheModule);
exports.CacheModule = CacheModule;


/***/ }),

/***/ "../../../libs/backend/cache/src/lib/cache.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


var _a;
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.CacheService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const cache_manager_1 = __webpack_require__("cache-manager");
let CacheService = class CacheService {
    constructor(cacheManager) {
        this.cacheManager = cacheManager;
    }
    getValueByKey(key) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.cacheManager.get(key);
        });
    }
    setValue(key, value, ttl = 0) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.cacheManager.set(key, value, ttl);
        });
    }
    deleteValueByKey(key) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.cacheManager.set(key, {}, 1);
        });
    }
    getStore() {
        return this.cacheManager.store;
    }
    clear() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            return yield this.cacheManager.reset();
        });
    }
};
CacheService = tslib_1.__decorate([
    (0, common_1.Injectable)(),
    tslib_1.__param(0, (0, common_1.Inject)(common_1.CACHE_MANAGER)),
    tslib_1.__metadata("design:paramtypes", [typeof (_a = typeof cache_manager_1.Cache !== "undefined" && cache_manager_1.Cache) === "function" ? _a : Object])
], CacheService);
exports.CacheService = CacheService;


/***/ }),

/***/ "../../../libs/backend/models/src/lib/auth/roles.ts":
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.Role = void 0;
var Role;
(function (Role) {
    Role[Role["Student"] = 1] = "Student";
    Role[Role["Teacher"] = 2] = "Teacher";
    Role[Role["Admin"] = 3] = "Admin";
})(Role = exports.Role || (exports.Role = {}));


/***/ }),

/***/ "../../../libs/backend/repository/src/index.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
tslib_1.__exportStar(__webpack_require__("../../../libs/backend/repository/src/lib/prisma.module.ts"), exports);


/***/ }),

/***/ "../../../libs/backend/repository/src/lib/prisma.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


// src/prisma/prisma.module.ts
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PrismaModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const prisma_service_1 = __webpack_require__("../../../libs/backend/repository/src/lib/prisma.service.ts");
let PrismaModule = class PrismaModule {
};
PrismaModule = tslib_1.__decorate([
    (0, common_1.Module)({
        providers: [prisma_service_1.PrismaService],
        exports: [prisma_service_1.PrismaService],
    })
], PrismaModule);
exports.PrismaModule = PrismaModule;


/***/ }),

/***/ "../../../libs/backend/repository/src/lib/prisma.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


// src/prisma/prisma.service.ts
Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.PrismaService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
const client_1 = __webpack_require__("@prisma/client");
let PrismaService = class PrismaService extends client_1.PrismaClient {
    enableShutdownHooks(app) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.$on('beforeExit', () => tslib_1.__awaiter(this, void 0, void 0, function* () {
                yield app.close();
            }));
        });
    }
};
PrismaService = tslib_1.__decorate([
    (0, common_1.Injectable)()
], PrismaService);
exports.PrismaService = PrismaService;


/***/ }),

/***/ "../../../libs/backend/system/src/index.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
tslib_1.__exportStar(__webpack_require__("../../../libs/backend/system/src/lib/system.module.ts"), exports);


/***/ }),

/***/ "../../../libs/backend/system/src/lib/date.service.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.DateService = void 0;
const tslib_1 = __webpack_require__("tslib");
const common_1 = __webpack_require__("@nestjs/common");
let DateService = class DateService {
    constructor() {
        this.toTimestamp = (strDate) => {
            const dt = new Date(strDate).getTime();
            return dt / 1000;
        };
    }
    compare(date1, date2) {
        return this.toTimestamp(date1) === this.toTimestamp(date2);
    }
};
DateService = tslib_1.__decorate([
    (0, common_1.Injectable)()
], DateService);
exports.DateService = DateService;


/***/ }),

/***/ "../../../libs/backend/system/src/lib/system.module.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.SystemModule = void 0;
const tslib_1 = __webpack_require__("tslib");
const date_service_1 = __webpack_require__("../../../libs/backend/system/src/lib/date.service.ts");
const common_1 = __webpack_require__("@nestjs/common");
let SystemModule = class SystemModule {
};
SystemModule = tslib_1.__decorate([
    (0, common_1.Module)({
        controllers: [],
        providers: [date_service_1.DateService],
        exports: [date_service_1.DateService]
    })
], SystemModule);
exports.SystemModule = SystemModule;


/***/ }),

/***/ "@nestjs/common":
/***/ ((module) => {

module.exports = require("@nestjs/common");

/***/ }),

/***/ "@nestjs/common/pipes/validation.pipe":
/***/ ((module) => {

module.exports = require("@nestjs/common/pipes/validation.pipe");

/***/ }),

/***/ "@nestjs/core":
/***/ ((module) => {

module.exports = require("@nestjs/core");

/***/ }),

/***/ "@nestjs/jwt":
/***/ ((module) => {

module.exports = require("@nestjs/jwt");

/***/ }),

/***/ "@nestjs/swagger":
/***/ ((module) => {

module.exports = require("@nestjs/swagger");

/***/ }),

/***/ "@prisma/client":
/***/ ((module) => {

module.exports = require("@prisma/client");

/***/ }),

/***/ "@rob3000/nestjs-kafka":
/***/ ((module) => {

module.exports = require("@rob3000/nestjs-kafka");

/***/ }),

/***/ "bcrypt":
/***/ ((module) => {

module.exports = require("bcrypt");

/***/ }),

/***/ "cache-manager":
/***/ ((module) => {

module.exports = require("cache-manager");

/***/ }),

/***/ "cache-manager-redis-yet":
/***/ ((module) => {

module.exports = require("cache-manager-redis-yet");

/***/ }),

/***/ "class-validator":
/***/ ((module) => {

module.exports = require("class-validator");

/***/ }),

/***/ "tslib":
/***/ ((module) => {

module.exports = require("tslib");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;

Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
const swagger_1 = __webpack_require__("@nestjs/swagger");
const app_module_1 = __webpack_require__("./src/app/app.module.ts");
const core_1 = __webpack_require__("@nestjs/core");
const validation_pipe_1 = __webpack_require__("@nestjs/common/pipes/validation.pipe");
function bootstrap() {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        const globalPrefix = "api";
        const config = new swagger_1.DocumentBuilder()
            .setTitle('Study Platform')
            .setDescription('Rakzin Roman API description')
            .setVersion('0.1')
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, config);
        swagger_1.SwaggerModule.setup('api', app, document);
        app.setGlobalPrefix(globalPrefix);
        app.useGlobalPipes(new validation_pipe_1.ValidationPipe());
        yield app.listen(3000);
    });
}
bootstrap();

})();

var __webpack_export_target__ = exports;
for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ })()
;
//# sourceMappingURL=main.js.map