import { Component } from '@angular/core';

@Component({
  selector: 'app-fe-admin-entry',
  template: `<app-nx-welcome></app-nx-welcome>`
})
export class RemoteEntryComponent {}
