import { NxWelcomeComponent } from './nx-welcome.component';
import { Route } from '@angular/router';

export const appRoutes: Route[] = [
    {
        path: 'fe-admin',
        loadChildren: () => import('fe-admin/Module').then(m => m.RemoteEntryModule)
    },
    {
        path: 'fe-teacher',
        loadChildren: () => import('fe-teacher/Module').then(m => m.RemoteEntryModule)
    },
    {
        path: 'fe-student',
        loadChildren: () => import('fe-student/Module').then(m => m.RemoteEntryModule)
    },
    {
        path: 'fe-landing',
        loadChildren: () => import('fe-landing/Module').then(m => m.RemoteEntryModule)
    },
    {
      path: '',
      component: NxWelcomeComponent
   
    }
];