import { ArticlesModule } from './modules/articles/articles.module';
import { AuthModule } from './modules/auth/auth.module';
import { CourseModule } from './modules/courses/courses.module';
import { Module } from '@nestjs/common';

@Module({
	imports: [
		AuthModule,
		ArticlesModule,
		CourseModule
	],
	controllers: [],
	providers: []
})
export class AppModule {}
