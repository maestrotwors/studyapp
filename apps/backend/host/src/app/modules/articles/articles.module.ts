import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { PrismaModule } from "@be-repository";
import { PrismaService } from "@be-repository/services/prisma.service";

@Module({
	controllers: [ArticlesController],
	providers: [ArticlesService, PrismaService],
	imports: [PrismaModule, AuthModule]
})
export class ArticlesModule {}
