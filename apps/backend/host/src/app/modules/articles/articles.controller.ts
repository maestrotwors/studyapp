import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Auth } from '../../decorators/auth.decorator';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { ArticleEntity } from './entities/article.entity';
import { Role } from '@be-models/auth/roles';

@Controller("articles")
@ApiTags("articles")
export class ArticlesController {
	constructor(private readonly articlesService: ArticlesService) {}

	@Post()
	@ApiCreatedResponse({ type: ArticleEntity })
	create(@Body() createArticleDto: CreateArticleDto) {
		return this.articlesService.create(createArticleDto);
	}

	@Get()
	//@UseGuards(AuthGuard)
	//@ApiOkResponse({ type: ArticleEntity, isArray: true })
	@ApiOkResponse({ type: ArticleEntity, isArray: true })
	@Auth([Role.Student])
	findAll() {
		return this.articlesService.findAll();
	}

	@Get("drafts")
	@ApiOkResponse({ type: ArticleEntity, isArray: true })
	findDrafts() {
		//return this.articlesService.findDrafts();
	}

	@Get(":id")
	@ApiOkResponse({ type: ArticleEntity })
	findOne(@Param("id") id: string) {
		return this.articlesService.findOne(+id);
	}

	@Patch(":id")
	@ApiOkResponse({ type: ArticleEntity })
	update(@Param("id") id: string, @Body() updateArticleDto: UpdateArticleDto) {
		return this.articlesService.update(+id, updateArticleDto);
	}

	@Delete(":id")
	@ApiOkResponse({ type: ArticleEntity })
	remove(@Param("id") id: string) {
		return this.articlesService.remove(+id);
	}
}



