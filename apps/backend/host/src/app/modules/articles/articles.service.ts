// src/articles/articles.service.ts

import { CreateArticleDto } from './dto/create-article.dto';
import { Injectable } from '@nestjs/common';
import { PrismaService } from "@be-repository/services/prisma.service";
import { UpdateArticleDto } from './dto/update-article.dto';

// src/articles/articles.service.ts

@Injectable()
export class ArticlesService {
  constructor(private prisma: PrismaService) {}

  create(createArticleDto: CreateArticleDto) {
    return this.prisma.article.create({ data: createArticleDto });  
  }

  findAll() {
    return this.prisma.article.findMany({ where: { published: true } });
  }

  findOne(id: number) {
    return this.prisma.article.findUnique({ where: { id } });
  }

  update(id: number, updateArticleDto: UpdateArticleDto) {
    return this.prisma.article.update({where: { id }, data: updateArticleDto});  
  }

  remove(id: number) {
    return this.prisma.article.delete({ where: { id } });  
  }
}