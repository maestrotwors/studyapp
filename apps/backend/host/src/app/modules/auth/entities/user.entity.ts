export class UserEntity {
	public name;
	public email;
	public login;
	public id;
	public password;
	public role?;
	public updatedAt = null;
}
