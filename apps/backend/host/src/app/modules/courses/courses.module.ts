//import { ClientsModule, Transport } from '@nestjs/microservices';

import { CourseController } from './courses.controller';
import { CoursesService } from './courses.service';
import { KafkaModule } from "@rob3000/nestjs-kafka";
import { Module } from "@nestjs/common";
import { env } from "@base/dev.env";

@Module({
	controllers: [CourseController],
	providers: [CoursesService],
	imports: [
		KafkaModule.register([
			{
				name: "HERO_SERVICE",
				options: {
					client: {
						clientId: "hero",
						brokers: [`${env.KAFKA_HOST}:${env.KAFKA_PORT}`]
					},
					consumer: {
						groupId: "hero-consumer",
						heartbeatInterval: 0
					}
				}
			}
		])
		/*ClientsModule.register([
			/*{
				name: "ITEM_MICROSERVICE",
				transport: Transport.TCP,
				options: { host: "::", port: 3010 }
			},
			{
				name: "KAFKA_MICROSERVICE",
				transport: Transport.KAFKA,
				options: {
					client: {
						clientId: "hero",
						brokers: ["localhost:29092"]
					},
					consumer: {
						groupId: "test-consumer"
					},
					producerOnlyMode: true
				}
			}
		])*/
	]
})
export class CourseModule {}
