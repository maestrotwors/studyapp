export class GetCoursesEvent {
	constructor(
		public readonly courceId: string
	) {}

	toString() {
		return JSON.stringify({
			courceId: this.courceId 
		});
	}
}