import { Controller, Get, Inject } from "@nestjs/common";
import { KafkaService } from '@rob3000/nestjs-kafka';

import { CoursesService } from './courses.service';

@Controller("course")
export class CourseController {
	constructor(
		private readonly coursesService: CoursesService,
		@Inject("HERO_SERVICE") private client: KafkaService
	) {}

	@Get("all")
	async getAllCourses() {
		const result = await this.client.send({
			topic: "hero.kill.dragon",
			messages: [
				{
					key: "1",
					value: "My messageeeeeeeeeeeee"
				}
			]
		});
		console.log(result);
		console.log('++++++');
		return this.coursesService.getAllCourses();
	}
}
