/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { AppModule } from './app/app.module';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from "@nestjs/microservices";

async function bootstrap() {
  const app = await NestFactory.createMicroservice(
		AppModule,
		/*{
			transport: Transport.TCP,
			options: {
				host: "::",
				port: 3010
			}
		},*/
		{
			name: "KAFKA_MICROSERVICE",
			transport: Transport.KAFKA,
			options: {
				client: {
					clientId: "hero2",
					brokers: ["localhost:29092"]
				},
				consumer: {
					groupId: "test-consumer"
				}
			}
		}
  );
  await app.listen();
  Logger.log(`🚀 Microservice courses is running`);
}

bootstrap();
