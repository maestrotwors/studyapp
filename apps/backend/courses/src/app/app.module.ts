import { AppController } from './app.controller';
import { CoursesService } from "./courses.service";
import { KafkaModule } from '@rob3000/nestjs-kafka';
import { Module } from '@nestjs/common';
import { env } from "@base/dev.env";

@Module({
	imports: [
		KafkaModule.register([
			{
				name: "HERO_SERVICE",
				options: {
					client: {
						clientId: "hero",
						brokers: [`${env.KAFKA_HOST}:${env.KAFKA_PORT}`]
					},
					consumer: {
						groupId: "hero-consumer",
						heartbeatInterval: 0
					}
				}
			}
		])
	],
	controllers: [AppController],
	providers: [CoursesService]
})
export class AppModule {}