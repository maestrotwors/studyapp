import { Controller, Get, Inject } from '@nestjs/common';
import { IHeaders, KafkaService, SubscribeTo } from '@rob3000/nestjs-kafka';

import { CoursesService } from "./courses.service";

@Controller()
export class AppController {
	constructor(private readonly coursesService: CoursesService, @Inject("HERO_SERVICE") private client: KafkaService) {}


	async onModuleInit() {
		this.client.subscribeToResponseOf("hero.kill.dragon", this);
		console.log('cources1111 init1');
	}

	@SubscribeTo('hero.kill.dragon')
	async getWorld(data: any, key: any, offset: number, timestamp: number, partition: number, headers: IHeaders): Promise<void> {
		console.log(data);
		console.log(key);
		console.log(offset);
		console.log(timestamp);
		console.log(partition);
		console.log(headers);
	}
}
