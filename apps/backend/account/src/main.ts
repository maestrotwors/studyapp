import { MicroserviceOptions, Transport } from "@nestjs/microservices";

import { AppModule } from "./app/app.module";
import { Logger } from "@nestjs/common";
import { NestFactory } from "@nestjs/core";

async function bootstrap() {
	const app = await NestFactory.createMicroservice<MicroserviceOptions>(AppModule, {
		transport: Transport.KAFKA,
		options: {
			client: {
				brokers: ["localhost:29092"]
			},
			consumer: {
				groupId: "test-consumer"
			}
		}
	});
	await app.listen();
	Logger.log(`🚀 Microservice account is running`);
}

bootstrap();
